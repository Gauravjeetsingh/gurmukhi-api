import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.hello.onCreated(function helloOnCreated() {

});

Template.hello.helpers({

});

Template.hello.events({
    'click button'(event, instance) {
        var obj;
        let count = 0;
        var array = [];
        console.log("hey");
        $.getJSON( "dictionary.json", function( data ) {
            $.each( data, function( key, val ) {
                console.log(key);
                obj = {
                    word: key,
                    definition : val
                }
                array.push(obj);
            });
            Meteor.call('dictionaryInsertion',array);
            console.log(array.length);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log( "error", textStatus , "this means", errorThrown);
        });
    },
});