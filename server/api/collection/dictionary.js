Dictionary= new Mongo.Collection("dictionary");

Dictionary.schema= new SimpleSchema({
 word: {
   type: String,
   label : 'english word',
 },
 definition: {
   type: String,
   label: 'definition for word',
 }
});
