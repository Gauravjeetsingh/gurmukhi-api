// Global API configuration
var param;
var Api = new Restivus({
    version: 'v1',
    apiPath: 'gurmukhi',
    useDefaultAuth: false,
    prettyJson: true,
    defaultHeaders: {
        'Content-Type': 'application/json'
    },
    enableCors : true,
});

// Generates: GET, POST on /api/items and GET, PUT, PATCH, DELETE on
// /api/items/:id for the Items collection
Api.addCollection(Dictionary,{authRequired : true, roleRequired : 'admin'});
// Generates: POST on /api/users and GET, DELETE /api/users/:id for
// Meteor.users collection
// Api.addCollection(Meteor.users, {
//   excludedEndpoints: ['getAll', 'put'],
//   routeOptions: {
//     authRequired: false
//   },
//   endpoints: {
//     post: {
//       authRequired: false
//     },
//     delete: {
//       roleRequired: 'admin'
//     }
//   }
// });

// Maps to: /api/articles/:id
// Api.addRoute('meaning/', {authRequired: false}, {
//   get: function () {
//     return Dictionary.find({});
//   },
// });
/*Api.addRoute('dictionary/meaning',{authRequired: true}, {
    get:{
        authRequired: false,
        action: function() {
            queryParams=this.queryParams.words.split(',');
            let data = {data: []};
            for(i=0; i< queryParams.length; i++){
                data.data.push(Dictionary.findOne({word : queryParams[i]},{fields : {_id:0}}));
            }
            return data;
        }
    }
});
*/

Api.addRoute('word/:akhar', {authRequired: false},{
    get: function(){
        var akhar = this.urlParams.akhar;
        var data = { word: akhar, definition: "" }
        var temp = Dictionary.findOne({word: akhar}, {fields : {_id:0}});
        console.log(temp);
        if(temp){
            data.definition = temp.definition;
        } else{
            data.definition = "Could not find the meaning.";
        }
        return data;
    }
});